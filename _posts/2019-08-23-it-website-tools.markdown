---
layout: post
title:  "Website Tools for IT Guys"
date:   2019-08-23 14:07:14 -0400
categories: website tools
---
This page contains a bunch of online tools for diagnosing and testing anything related to a network or a website, or tools for checking or converting data or code. This will be updated often so be sure to check back occasionally.

Is your website configured right?
------
Here are some tools for checking the configuration of your website and testing other things.

Check MX
==================
The [Check MX][g-suite-check-mx] tool from Google's G Suite Toolbox can be used to check your website's MX and SPF records.

On this page, there are two inputs: Domain name and DKIM selector. The domain name input is for the domain you are wanting to test. Subdomains are able to be checked. The DKIM selector input is optional, and is used for checking the SPF records. Once you fill in the appropriate information, either press Enter or click on the "RUN CHECKS!" button.

Most websites, even if they aren't owned by you, can checked. The main exceptions are domains owned by Google. You will get an error saying that it cannot be checked and that they are configured just fine.

This checks your domain's DNS configuration for the following things:
* Domain should have at least 2 NS servers.
* Naked domain must be an A record (not CNAME).
* Every name server must reply with exactly the same TXT DKIM records.
* Every name server must reply with exactly the same TXT DMARC records.
* Every name server must reply with exactly the same CNAME records.
* Every name server must reply with exactly the same NS records.
* Every name server must reply with exactly the same MX records.
* Every name server must reply with exactly the same TXT records.
* DKIM authentication DNS setup.
* Formatting of DMARC policies.
* SPF must allow Google servers to send mail on behalf of your domain.
* MX lookup must fit in one UDP response packet.

This tool is meant for those whose websites are connected to G Suite. However it can still be used to check any website for the above things. Depending on your set up and purpose, some of the checks may not apply to you.

Dig
==================
The [Dig][g-suite-dig] tool from Google's G Suite Toolbox can be used to run a DNS lookup on your website to see what goes where.

It has a single input box for the domain name, which will automatically search as you type. Leave out the scheme, port and path; only the domain name (with the optional subdomain(s)) is allowed. Unlike the Check MX tool, this can be used with Google's domains

This tool allows you to check the following DNS records: A, AAAA, ANY, CAA, CNAME, MX, NS, PTR, SOA, SRV, and TXT. If some of the tabs are not showing an answer, experiment with different inputs; e.g. if you looked up `www.example.com`, and the answer came back empty, change the input to `example.com` or try a different subdomain.

The responses consist of `id`, `opcode`, `rcode`, `flags`, the question (found after `;Question`), the answer (bolded, found after `;Answer`), `;Authority`, and `;Additional`.

Browserinfo
==================
The [Browserinfo][g-suite-browserinfo] tool from Google's G Suite Toolbox can be used to show you information about your browser. It shows you the following information:
* Your IP - displays your IP address; it showed mine in the IPv6 format
* User Agent - the User Agent of the browser
* Time, according to the browser - the current time that your browser reports through the JavaScript API
* Timezone Offset - Your timezone offset from GMT
* Language - The language your browser is configured with, as reflected in its request headers.
* Plugins
* Cookies - Whether they are enabled or disabled
* Screen Resolution - Your screen's resolution
* Encoding - From the request headers
* Keep Alive - From the request headers
* Firewall

This information can be useful to show you what websites can learn about you from the network requests they receive from your browser.

HAR Analyzer
==================
[HAR Analyzer][g-suite-har-analyzer]

Log Analyzer
==================
[Log Analyzer][g-suite-log-analyzer] and [Log Analyzer 2][g-suite-log-analyzer-2]

Message Header
==================
[Message Header][g-suite-messageheader]

Encode Decode
==================
[Encode Decode][g-suite-encode-decode]

Public DNS Flush Cache
==================
[Flush Cache][google-public-dns-flush-cache]

WebRTC Troubleshooter
==================
[WebRTC Troubleshooter][webrtc-troubleshooter]

DNS Leak Test
==================
[DNS Leak Test][dns-leak-test]
{% comment %}
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
{% endcomment %}
[g-suite-check-mx]: https://toolbox.googleapps.com/apps/checkmx/
[g-suite-dig]: https://toolbox.googleapps.com/apps/dig/
[g-suite-browserinfo]: https://toolbox.googleapps.com/apps/browserinfo/
[g-suite-har-analyzer]: https://toolbox.googleapps.com/apps/har_analyzer/
[g-suite-log-analyzer]: https://toolbox.googleapps.com/apps/loganalyzer/
[g-suite-log-analyzer-2]: https://toolbox.googleapps.com/apps/loggershark/
[g-suite-messageheader]: https://toolbox.googleapps.com/apps/messageheader/
[g-suite-encode-decode]: https://toolbox.googleapps.com/apps/encode_decode/
[google-public-dns-flush-cache]: https://developers.google.com/speed/public-dns/cache
[webrtc-troubleshooter]: https://test.webrtc.org/
[dns-leak-test]: https://dnsleaktest.com/